<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
    <title>
      <fmt:message key="catalog.title" />
    </title>
    <c:import url="parts/header.jsp" />
  </head><%@ include file="parts/navigation.jsp" %> <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding gray">
      <!-- Main Container -->
      <div class="container">
        <h3>
          <fmt:message key="catalog.title" />
        </h3>
        <!-- Row -->
        <div class="row">
          <!-- Middle Content Area -->
          <div class="col-md-4 col-sm-12 col-xs-12 leftbar-stick blog-sidebar">
            <div class="user-profile">
              <ul>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=detective">
                    <fmt:message key="catalog.detective" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=fantastic">
                    <fmt:message key="catalog.fantastic" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=adventure">
                    <fmt:message key="catalog.adventure" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=novel">
                    <fmt:message key="catalog.novel" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=scientific">
                    <fmt:message key="catalog.scientific" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=children">
                    <fmt:message key="catalog.children" />
                  </a>
                </li>
                <li>
                  <a href="?command=FILTER_BOOKS&genreParam=educational">
                    <fmt:message key="catalog.educational" />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-md-8 col-sm-12 col-xs-12">
            <!-- Row -->
            <div class="row">
              <!-- Sorting Filters -->
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">
                <c:if test="${userRole == 'ADMIN'}">
                  <ul class="menu-search-bar" style="float: right;">
                    <li>
                      <a href="?command=SHOW_ADD_BOOK_PAGE" class="btn btn-light">
                        <i class="fa fa-plus" aria-hidden="true"></i>
                        <fmt:message key="catalog.add_button" />
                      </a>
                    </li>
                  </ul>
                </c:if>
              </div>
              <!-- Sorting Filters End-->
              <div class="clearfix"></div>
              <div class="posts-masonry">
                <div class="col-md-12 col-xs-12 col-sm-12 user-archives">
                  <c:if test="${not empty allBooks}">
                    <div class="filter-brudcrums">
                      <div class="filter-brudcrums-sort">
                        <ul>
                          <li>
                            <span>Sort by:</span>
                          </li>
                          <li>
                            <a href="?command=SORT_BOOKS&bookSortParam=name">
                              <fmt:message key="catalog.name_of_book" />
                            </a>
                          </li>
                          <li>
                            <a href="?command=SORT_BOOKS&bookSortParam=quantity">
                              <fmt:message key="catalog.quantity" />
                            </a>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </c:if>
                                        <div class="col-lg-9 col-md-9 col-sm-9 no-padding">
                                    <c:if test="${empty allBooks && allBooks.size() == 0}"> No data </c:if>
                                           <c:choose>
                                                <c:when test="${not empty sessionScope.error}">
                                                    <div class = "col-6">
                                                        <div class="alert alert-danger alert-dismissible">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                            ${error}
                                                        </div>
                                                        <%session.setAttribute("error", "");%>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${not empty requestScope.error}">
                                                    <div class = "col-6">
                                                        <div class="alert alert-danger alert-dismissible">
                                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                            ${error}
                                                        </div>
                                                    </div>
                                                </c:when>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${not empty requestScope.successMakeOrder}">
                                                    <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${sessionScope.successMakeOrder == true}">
                                                        <div class = "col-6">
                                                            <div class="alert alert-success alert-dismissible">
                                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                <fmt:message key="alert.success_make_order"/> "${sessionScope.orderNameBook}"!
                                                            </div>
                                                            <%session.setAttribute("successMakeOrder", false);%>
                                                        </div>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                            <c:choose>
                                                <c:when test="${not empty requestScope.successEditBook}">
                                                    <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                                                </c:when>
                                                <c:otherwise>
                                                    <c:if test="${sessionScope.successEditBook == true}">
                                                        <div class = "col-6">
                                                            <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                                                <fmt:message key="alert.success_edit_book"/>
                                                            </div>
                                                            <%session.setAttribute("successEditBook", false);%>
                                                        </div>
                                                    </c:if>
                                                </c:otherwise>
                                            </c:choose>
                                  </div>
                  <c:forEach var="book" items="${sessionScope.allBooks}">
                    <div class="ads-list-archive">
                      <!-- Image Block -->
                      <div class="col-lg-3 col-md-3 col-sm-3 no-padding">
                        <!-- Img Block -->
                        <div class="ad-archive-img">
                          <a href="#">
                            <img src="${pageContext.servletContext.contextPath}/static/images/posting/2.jpg" alt="">
                          </a>
                        </div>
                        <!-- Img Block -->
                      </div>
                      <!-- Ads Listing -->
                      <div class="clearfix visible-xs-block"></div>
                      <!-- Content Block -->
                      <div class="col-lg-9 col-md-9 col-sm-9 no-padding">
                        <!-- Ad Desc -->
                        <div class="ad-archive-desc">
                          <!-- Title -->
                          <h3> ${book.name} </h3>
                          <!-- Category -->
                          <div class="category-title">
                            <span>${book.genre}</span>
                          </div>
                          <div class="category-title">Author: <span>${book.author}</span></div>
                          <div class="category-title"> <span>${book.publishingHouse}</span></div>
                          <div class="category-title">Page: <span>${book.numberOfPage}</span></div>
                          <div class="category-title">Quantity: <span>${book.quantity}</span></div>
                          <div class="clearfix visible-xs-block"></div>
                          <!-- Ad Features -->
                          <!-- Ad History -->
                          <div class="clearfix archive-history">
                            <div class="ad-meta">
                              <c:if test="${userRole == 'USER'}">
                                <c:if test="${book.quantity > 0}">
                                  <a href="?command=MAKE_ORDER&bookId=${book.id}" class="btn save-ad">
                                    <fmt:message key="catalog.order_button" />
                                  </a>
                                </c:if>
                              </c:if>
                              <c:if test="${userRole == 'ADMIN'}">
                                <a href="?command=SHOW_EDIT_BOOK_PAGE&bookId=${book.id}" class="btn save-ad">
                                  <fmt:message key="catalog.edit_button" />
                                </a>
                              </c:if>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </c:forEach>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>