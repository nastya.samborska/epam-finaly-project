<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:choose>
  <c:when test="${not empty language}">
    <fmt:setLocale value="${sessionScope.language}" />
  </c:when>
  <c:when test="${empty language}">
    <fmt:setLocale value="en" />
  </c:when>
</c:choose>
<fmt:setBundle basename="language" />
<head>
  <title><fmt:message key="sign_in_page.title"/></title>
  <c:import url="parts/header.jsp"/>
</head>
<%@ include file="parts/navigation.jsp" %>
<div class="small-breadcrumb">
  <div class="container">
    <div class=" breadcrumb-link">
      <ul><li>&nbsp;</li></ul>
    </div>
  </div>
</div>
<div class="main-content-area clearfix">
  <section class="section-padding error-page pattern-bg ">
    <!-- Main Container -->
    <div class="container">
    <h3><fmt:message key="header.sign_in"/></h3>
      <div class="row">
        <c:choose>
          <c:when test="${not empty requestScope.successRegister}">
            <p>${requestScope.error}</p>
            <a href="${pageContext.request.contextPath}/controller?command=SHOW_LOGIN_PAGE">Try again</a>
          </c:when>
          <c:otherwise>
            <c:if test="${sessionScope.successRegister == true}">
              <div class = "col-6">
                <div class="alert alert-success alert-dismissible">
                  <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                  <fmt:message key="alert.success_registration"/>
                </div>
                <%session.setAttribute("successRegister", false);%>
              </div>
            </c:if>
          </c:otherwise>
        </c:choose>
        <c:choose>
          <c:when test="${not empty requestScope.wrongData}">
            <div class = "col-6">
              <div class="alert alert-danger alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                <fmt:message key="alert.wrong_data_login"/>
              </div>
            </div>
          </c:when>
        </c:choose>
        <div class="col-md-5 col-md-push-3 col-sm-6 col-xs-12">
          <div class="form-grid">
            <form id="login-form" class="form" action="final-project-epam" method="post">
              <div class="form-group">
                <label><fmt:message key="sign_in_page.login"/></label>
                <input placeholder="<fmt:message key="sign_in_page.login"/>" class="form-control" required autofocus name="login" type="text">
              </div>
              <div class="form-group">
                <label><fmt:message key="sign_in_page.password"/></label>
                <input placeholder="<fmt:message key="sign_in_page.password"/>" class="form-control" required name="password" type="password">
              </div>
              <div class="form-group">
                <div class="row">
                  <div class="col-xs-12">
                    <div class="skin-minimal">
                      <ul class="list">
                        <li>
                          <input  type="checkbox" id="minimal-checkbox-1">
                          <label for="minimal-checkbox-1">Remember Me</label>
                        </li>
                      </ul>
                    </div>
                  </div>
                </div>
              </div>
                 <input type="hidden" name="command" value="LOGIN"/>
                                                      <input type="submit" class="btn btn-theme btn-lg btn-block"" value=
                                                      <fmt:message key="sign_in_page.sign_in"/>
                                                      >
            </form>
          </div>
          <!-- Form -->
        </div>

      </div>
      <!-- Row End -->
    </div>
    <!-- Main Container End -->
  </section>
  <c:import url="parts/footer.jsp"/>
</div>
</body>
</html>