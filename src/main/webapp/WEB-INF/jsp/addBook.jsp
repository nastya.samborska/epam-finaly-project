<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
    <title>
      <fmt:message key="sign_in_page.title" />
    </title>
    <c:import url="parts/header.jsp" />
  </head>
  <%@ include file="parts/navigation.jsp" %>
   <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding error-page pattern-bg ">
      <!-- Main Container -->
      <div class="container">
           <h3>Add book</h3>
        <!-- Row -->
        <div class="row">
          <!-- Middle Content Area -->
          <div class="col-md-5 col-md-push-3 col-sm-6 col-xs-12">
            <!--  Form -->
            <div class="form-grid">
              <form id="login-form" class="form" action="final-project-epam" method="post">
                <div class="form-group">
                  <label for="name" class="text-light">
                    <fmt:message key="add_book.name_of_book" />
                  </label>
                  <br>
                  <input type="text" name="name" id="name" class="form-control" required />
                </div>
                <div class="form-group">
                  <label for="author" class="text-light">
                    <fmt:message key="add_book.author" />
                  </label>
                  <br>
                  <input type="text" name="author" id="author" class="form-control" required />
                </div>
                <div class="form-group">
                  <label for="publisher" class="text-light">
                    <fmt:message key="add_book.publishing_house" />
                  </label>
                  <br>
                  <input type="text" name="publisher" id="publisher" class="form-control" required />
                </div>
                <div class="form-group">
                  <span class="form-label">
                    <fmt:message key="add_book.genre" />
                  </span>
                  <select class="form-control" required name="genre">
                    <option value="" selected hidden>
                      <fmt:message key="add_book.select_genre" />
                    </option>
                    <option value="1">
                      <fmt:message key="add_book.detective" />
                    </option>
                    <option value="2">
                      <fmt:message key="add_book.fantastic" />
                    </option>
                    <option value="3">
                      <fmt:message key="add_book.adventure" />
                    </option>
                    <option value="4">
                      <fmt:message key="add_book.novel" />
                    </option>
                    <option value="5">
                      <fmt:message key="add_book.scientific" />
                    </option>
                    <option value="6">
                      <fmt:message key="add_book.children" />
                    </option>
                    <option value="7">
                      <fmt:message key="add_book.educational" />
                    </option>
                  </select>
                  <span class="select-arrow"></span>
                </div>
                <div class="form-group">
                  <label for="yearPublishing" class="text-light">
                    <fmt:message key="add_book.year_publishing" />
                  </label>
                  <br>
                  <input type="text" name="yearPublishing" id="yearPublishing" class="form-control" required />
                </div>
                <div class="form-group">
                  <label for="pages" class="text-light">
                    <fmt:message key="add_book.number_of_page" />
                  </label>
                  <br>
                  <input type="text" name="pages" id="pages" class="form-control" required "/>
                </div>
                <div class="form-group">
                  <label for="quantity" class="text-light">
                    <fmt:message key="add_book.quantity" />
                  </label>
                  <br>
                  <input type="text" name="quantity" id="quantity" class="form-control" required"/>
                </div>
                <div style="color: red;">
                  <p>
               <c:choose>
                         <c:when test="${not empty requestScope.error}">
                             <div class = "col-3">
                                 <div class="alert alert-danger alert-dismissible">
                                     <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                     ${error}
                                 </div>
                             </div>
                         </c:when>
                     </c:choose>
                     <c:choose>
                         <c:when test="${not empty requestScope.successAddBook}">
                             <p>${requestScope.error}</p>
                             <a href="${pageContext.request.contextPath}/controller?command=SHOW_ADD_BOOK_PAGE">Try again</a>
                         </c:when>
                         <c:otherwise>
                             <c:if test="${sessionScope.successAddBook == true}">
                               <div class = "col-3">
                                     <div class="alert alert-success alert-dismissible">
                                         <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                         <fmt:message key="alert.success_add_book"/>
                                     </div>
                                     <%session.setAttribute("successAddBook", false);%>
                                 </div>
                             </c:if>
                         </c:otherwise>
                     </c:choose>
                  </p>
                </div>
                <input type="hidden" name="command" value="ADD_BOOK" />
                <input type="submit" class="btn btn-theme btn-lg btn-block" value=<fmt:message key="add_book.add_button"/>
                                                                                                                >
              </form>
            </div>
            <!-- Form -->
          </div>
        </div>
        <!-- Row End -->
      </div>
      <!-- Main Container End -->
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>