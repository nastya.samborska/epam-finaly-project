<%@ page contentType="text/html;charset=UTF-8" language="java" %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %><%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %> <div class="colored-header">
  <!-- Top Bar -->
  <div class="header-top">
    <div class="container">
      <div class="row">
        <div class="header-top-left col-md-8 col-sm-6 col-xs-12 hidden-xs">
          <div class="pull-left">
            <ul class="listnone">
                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                      <c:choose>
                        <c:when test="${sessionScope.language == 'uk'}">
                          <fmt:message key="header.language_uk" />
                        </c:when>
                        <c:when test="${sessionScope.language == 'en'}">
                          <fmt:message key="header.language_en" />
                        </c:when>
                        <c:otherwise>
                          <fmt:message key="header.language_en" />
                        </c:otherwise>
                      </c:choose>
                      <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <a class="nav-link" href="/final-project-epam?command=CHANGE_LANGUAGE&language=uk">
                          <span>
                            <fmt:message key="header.language_uk" />
                          </span>
                        </a>
                      </li>
                      <li>
                        <a class="nav-link" href="/final-project-epam?command=CHANGE_LANGUAGE&language=en">
                          <span>
                            <fmt:message key="header.language_en" />
                          </span>
                        </a>
                      </li>
                    </ul>
                  </li>
            </ul>
          </div>
        </div>
        <div class="header-right col-md-4 col-sm-6 col-xs-12 ">
          <div class="pull-right">
            <ul class="listnone">
              <c:choose>
                <c:when test="${not empty user}">
                  <li>
                  <span class="user-name-select__label">
                                                  ${sessionScope.account.firstName} ${sessionScope.account.secondName}
                                              </span>
                  </li>
                     <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                      <i class="icon-profile-male" aria-hidden="true"></i> ${sessionScope.user_name} <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu">
                      <li>
                        <a href="/final-project-epam?command=SHOW_EDIT_ACCOUNT_PAGE">
                          <fmt:message key="header.settings" />
                        </a>
                      </li>
                      <li>
                        <a class="login-btn" href="final-project-epam?command=LOGOUT">
                          <fmt:message key="header.log_out" />
                        </a>
                      </li>
                    </ul>
                  </li>
                </c:when>
                <c:otherwise>
                  <li>
                    <a href="/final-project-epam?command=SHOW_LOGIN_PAGE">
                      <i class="fa fa-sign-in"></i>
                      <fmt:message key="header.sign_in" />
                    </a>
                  </li>
                  <li>
                    <a href="/final-project-epam?command=SHOW_REGISTRATION_PAGE">
                      <i class="fa fa-unlock" aria-hidden="true"></i>
                      <fmt:message key="header.register" />
                    </a>
                  </li>
                </c:otherwise>
              </c:choose>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Top Bar End -->
  <!-- Navigation Menu -->
  <div class="clearfix"></div>
  <!-- menu start -->
  <nav id="menu-1" class="mega-menu">
    <!-- menu list items container -->
    <section class="menu-list-items">
      <div class="container">
        <div class="row">
          <div class="col-lg-12 col-md-12">
            <!-- menu links -->
            <ul class="menu-links">
              <!-- active class -->
              <li>
                <a href="/final-project-epam?command=SHOW_MAIN_PAGE">
                  <fmt:message key="header.home" />
                </a>
              </li>
              <c:if test="${not empty user}">
                <c:if test="${userRole == 'USER'}">
                  <li>
                    <a href="/final-project-epam?command=SHOW_ALL_BOOKS">
                      <fmt:message key="header.catalog" />
                    </a>
                  </li>
                  <li>
                    <a href="/final-project-epam?command=SHOW_USER_ORDERS">
                      <fmt:message key="header.orders" />
                    </a>
                  </li>
                </c:if>
                <c:if test="${userRole == 'ADMIN'}">
                  <li>
                    <a href="/final-project-epam?command=SHOW_ALL_BOOKS">
                      <fmt:message key="header.catalog" />
                    </a>
                  </li>
                  <li>
                    <a href="/final-project-epam?command=SHOW_ALL_USERS">
                      <fmt:message key="header.users" />
                    </a>
                  </li>
                  <li>
                    <a href="/final-project-epam?command=SHOW_ALL_ORDERS">
                      <fmt:message key="header.orders" />
                    </a>
                  </li>
                </c:if>
              </c:if>
            </ul>
          </div>
        </div>
      </div>
    </section>
  </nav>
</div>