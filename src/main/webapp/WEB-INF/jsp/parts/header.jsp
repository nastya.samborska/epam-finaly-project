<%@ taglib prefix="c" uri="http://java.sun.com/jstl/core" %>

<!-- =-=-=-=-=-=-= Mobile Specific =-=-=-=-=-=-= -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- =-=-=-=-=-=-= Bootstrap CSS Style =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/bootstrap.css'/>">
<!-- =-=-=-=-=-=-= Template CSS Style =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/style.css'/>">
<!-- =-=-=-=-=-=-= Font Awesome =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/font-awesome.css'/>" type="text/css">
<!-- =-=-=-=-=-=-= Flat Icon =-=-=-=-=-=-= -->
<link href="<c:url value='/static/css/flaticon.css'/>" rel="stylesheet">
<!-- =-=-=-=-=-=-= Et Line Fonts =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/et-line-fonts.css'/>" type="text/css">
<!-- =-=-=-=-=-=-= Menu Drop Down =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/forest-menu.css'/>" type="text/css">
<!-- =-=-=-=-=-=-= Animation =-=-=-=-=-=-= -->
<link rel="stylesheet" href="<c:url value='/static/css/animate.min.css'/>" type="text/css">
<!-- =-=-=-=-=-=-= Select Options =-=-=-=-=-=-= -->
<link href="<c:url value='/static/css/select2.min.css'/>" rel="stylesheet" />
<!-- =-=-=-=-=-=-= Check boxes =-=-=-=-=-=-= -->
<link href="<c:url value='/static/skins/minimal/minimal.css'/>" rel="stylesheet">
<!-- =-=-=-=-=-=-= Responsive Media =-=-=-=-=-=-= -->
<link href="<c:url value='/static/css/responsive-media.css'/>" rel="stylesheet">
<!-- =-=-=-=-=-=-= Template Color =-=-=-=-=-=-= -->
<link rel="stylesheet" id="color" href="<c:url value='/static/css/defualt.css'/>">

<!-- JavaScripts -->
<script src="<c:url value='/static/js/modernizr.js'/>"></script>
