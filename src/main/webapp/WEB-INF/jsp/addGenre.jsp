<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:choose>
    <c:when test="${not empty language}">
        <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
        <fmt:setLocale value="en" />
    </c:when>
</c:choose>
<fmt:setBundle basename="language" />
<head>
    <title>
        <fmt:message key="sign_in_page.title" />
    </title>
    <c:import url="parts/header.jsp" />
</head>
<%@ include file="parts/navigation.jsp" %>
<div class="small-breadcrumb">
    <div class="container">
        <div class=" breadcrumb-link">
            <ul>
                <li>&nbsp;</li>
            </ul>
        </div>
    </div>
</div>
<div class="main-content-area clearfix">
    <section class="section-padding error-page pattern-bg ">
        <!-- Main Container -->
        <div class="container">
            <h3>Add genre</h3>
            <!-- Row -->
            <div class="row">
                <!-- Middle Content Area -->
                <div class="col-md-5 col-md-push-3 col-sm-6 col-xs-12">
                    <!--  Form -->
                    <div class="form-grid">
                        <form id="login-form" class="form" action="final-project-epam" method="post">
                            <div class="form-group">
                                <label for="name" class="text-light">
                                    <fmt:message key="add_genre.name_of_genre" />
                                </label>
                                <br>
                                <input type="text" name="name" id="name" class="form-control" required />
                            </div>
                            <input type="hidden" name="command" value="ADD_BOOK" />
                            <input type="submit" class="btn btn-theme btn-lg btn-block" value=<fmt:message key="add_book.add_button"/>
                            >
                        </form>
                    </div>
                    <!-- Form -->
                </div>
            </div>
            <!-- Row End -->
        </div>
        <!-- Main Container End -->
    </section>
    <c:import url="parts/footer.jsp" />
</div>
</body>
</html>