<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
    <title><fmt:message key="orders.title_orders"/></title>
    <c:import url="parts/header.jsp" />
  </head>
  <%@ include file="parts/navigation.jsp" %> <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding gray">
      <!-- Main Container -->
      <div class="container">
        <h3>
           <fmt:message key="orders.title_orders"/>
        </h3>
        <!-- Row -->
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- Row -->
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">

                  <c:if test="${empty allOrders && allOrders.size() == 0}"> No data </c:if>
                         <c:choose>
                              <c:when test="${not empty sessionScope.error}">
                                  <div class = "container p-3">
                                      <div class="alert alert-danger alert-dismissible" style="width: 500px; margin:auto">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          ${error}
                                      </div>
                                      <%session.setAttribute("error", "");%>
                                  </div>
                              </c:when>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.error}">
                                  <div class = "container p-3">
                                      <div class="alert alert-danger alert-dismissible" style="width: 500px; margin:auto">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          ${error}
                                      </div>
                                  </div>
                              </c:when>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.successMakeOrder}">
                                  <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                              </c:when>
                              <c:otherwise>
                                  <c:if test="${sessionScope.successMakeOrder == true}">
                                      <div class = "container p-3">
                                          <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                              <fmt:message key="alert.success_make_order"/> "${sessionScope.orderNameBook}"!
                                          </div>
                                          <%session.setAttribute("successMakeOrder", false);%>
                                      </div>
                                  </c:if>
                              </c:otherwise>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.successEditBook}">
                                  <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                              </c:when>
                              <c:otherwise>
                                  <c:if test="${sessionScope.successEditBook == true}">
                                      <div class = "container p-3">
                                          <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                              <fmt:message key="alert.success_edit_book"/>
                                          </div>
                                          <%session.setAttribute("successEditBook", false);%>
                                      </div>
                                  </c:if>
                              </c:otherwise>
                          </c:choose>


                           <table class="table table-striped">
                                                  <c:choose>
                                                      <c:when test="${empty allOrders && allOrders.size() == 0}">
                                                          <tr>
                                                              <td><fmt:message key="catalog.not_found_message"/></td>
                                                          </tr>
                                                      </c:when>
                                                      <c:when test="${not empty allOrders}">
                                                          <thead>
                                                          <tr>
                                                              <th>№</th>
                                                              <th><fmt:message key="orders.login"/></th>
                                                              <th><fmt:message key="orders.name_book"/></th>
                                                              <th><fmt:message key="orders.author"/></th>
                                                              <th><fmt:message key="orders.order_date"/></th>
                                                              <th><fmt:message key="orders.return_date"/></th>
                                                              <th><fmt:message key="orders.subscription"/></th>
                                                              <th><fmt:message key="orders.status"/></th>
                                                              <th></th>
                                                          </tr>
                                                          </thead>
                                                          <tbody>
                                                          <c:forEach items="${sessionScope.allOrders}" var="order">
                                                              <tr>
                                                                  <td>${order.id}</td>
                                                                  <td>${order.account.user.login}</td>
                                                                  <td>${order.book.name}</td>
                                                                  <td>${order.book.author}</td>
                                                                  <td>${order.dateOfIssue}</td>
                                                                  <td>${order.returnDate}</td>
                                                                  <c:if test="${order.subscription == true}">
                                                                      <td>${order.account.subscriptionId}</td>
                                                                  </c:if>
                                                                  <c:if test="${order.subscription == false}">
                                                                      <td>-</td>
                                                                  </c:if>
                                                                  <td>${order.orderStatus}</td>
                                                                  <td class="rooms-table__action">
                                                                      <c:if test="${order.orderStatus eq 'WAITING'}">
                                                                          <a href="final-project-epam/?command=CANCEL_ORDER&orderId=${order.id}"
                                                                             class="rooms-table__button">
                                                                              <fmt:message key="orders.cancel_button"/>
                                                                          </a>
                                                                          <a href="final-project-epam/?command=CONFIRM_ORDER&orderId=${order.id}"
                                                                             class="rooms-table__button">
                                                                              <fmt:message key="orders.confirm_button"/>
                                                                          </a>
                                                                      </c:if>
                                                                      <c:if test="${order.orderStatus eq 'ACTIVE'}">
                                                                          <a href="final-project-epam/?command=RETURN_BOOK&orderId=${order.id}"
                                                                             class="rooms-table__button">
                                                                              <fmt:message key="orders.return_button"/>
                                                                          </a>
                                                                      </c:if>
                                                                  </td>
                                                              </tr>
                                                          </c:forEach>
                                                      </c:when>
                                                  </c:choose>
                                              </tbody>
                                              </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>