<%@ page contentType="text/html;charset=UTF-8" language="java" %><%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %><%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
   <title><fmt:message key="edit_account.title"/></title>
    <c:import url="parts/header.jsp" />
  </head><%@ include file="parts/navigation.jsp" %> <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding error-page pattern-bg ">
      <!-- Main Container -->
      <div class="container">
      <h3><fmt:message key="edit_account.title"/></h3>
        <!-- Row -->
        <div class="row">
          <!-- Middle Content Area -->
          <div class="col-md-5 col-md-push-3 col-sm-6 col-xs-12">
            <!--  Form -->
                    <c:choose>
                        <c:when test="${not empty requestScope.successEditAccount}">
                            <p>${requestScope.error}</p>
                            <a href="${pageContext.request.contextPath}/controller?command=SHOW_MAIN_PAGE">Try again</a>
                        </c:when>
                        <c:otherwise>
                            <c:if test="${sessionScope.successEditAccount == true}">
                                <div class = "container p-3">
                                    <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        <fmt:message key="alert.success_edit_account"/>
                                    </div>
                                    <%session.setAttribute("successEditAccount", false);%>
                                </div>
                            </c:if>
                        </c:otherwise>
                    </c:choose>
                    <c:choose>
                        <c:when test="${not empty requestScope.error}">
                            <div class = "container p-3">
                                <div class="alert alert-danger alert-dismissible" style="width: 500px; margin:auto">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    ${error}
                                </div>
                            </div>
                        </c:when>
                    </c:choose>
            <div class="form-grid">
                        <form class="form" action="library" method="post">

                                             <div class="form-group">
                                                 <label for="firstName" class="text-light"><fmt:message key="edit_account.first_name"/></label><br>
                                                 <input type="text" name="firstName" id="firstName" class="form-control"
                                                        value="${sessionScope.account.firstName}" required/>
                                             </div>

                                             <div class="form-group">
                                                 <label for="secondName" class="text-light"><fmt:message key="edit_account.second_name"/></label><br>
                                                 <input type="text" name="secondName" id="secondName" class="form-control"
                                                        value="${sessionScope.account.secondName}" required />
                                             </div>

                                             <div class="form-group">
                                                 <label for="phone" class="text-light">email</label><br>
                                                 <input type="email" name="email" id="email" class="form-control"
                                                        value="${sessionScope.account.email}" required/>
                                             </div>

                                             <div class="form-group">
                                                 <label for="phone" class="text-light"><fmt:message key="edit_account.phone"/></label><br>
                                                 <input type="text" name="phone" id="phone" class="form-control"
                                                        value="${sessionScope.account.phone}" required/>
                                             </div>

                                             <div class="form-group">
                                                 <label for="subscriptionId" class="text-light"><fmt:message key="edit_account.subscription"/></label><br>
                                                 <c:if test="${not empty sessionScope.account.subscriptionId}">
                                                     <input type="text" name="subscriptionId" id="subscriptionId" class="form-control"
                                                            value="${sessionScope.account.subscriptionId}" required/>
                                                 </c:if>
                                                 <c:if test="${empty sessionScope.account.subscriptionId}">
                                                     <input type="text" name="subscriptionId" id="subscriptionId" class="form-control"
                                                            />
                                                 </c:if>
                                             </div>


                                             <div class="form-group">
                                                 <input type="hidden" name="command" value="EDIT_ACCOUNT"/>
                                                 <input type="submit" class="btn btn-primary btn-md" value=
                                                         <fmt:message key="edit_account.edit_button"/>
                                                 >
                                                 <a href="final-project-epam?command=SHOW_MAIN_PAGE"
                                                    class="btn btn-secondary">
                                                     <fmt:message key="edit_account.cancel_button"/>
                                                 </a>
                                             </div>
              </form>
            </div>
            <!-- Form -->
          </div>
        </div>
        <!-- Row End -->
      </div>
      <!-- Main Container End -->
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>