<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<c:choose>
  <c:when test="${not empty language}">
    <fmt:setLocale value="${sessionScope.language}" />
  </c:when>
  <c:when test="${empty language}">
    <fmt:setLocale value="en" />
  </c:when>
</c:choose>
<fmt:setBundle basename="language" />
<head>
<title><fmt:message key="registration_page.title"/></title>
  <c:import url="parts/header.jsp"/>
</head>
<%@ include file="parts/navigation.jsp" %>
<div class="small-breadcrumb">
  <div class="container">
    <div class=" breadcrumb-link">
      <ul><li>&nbsp;</li></ul>
    </div>
  </div>
</div>
<div class="main-content-area clearfix">
  <section class="section-padding error-page pattern-bg ">
    <!-- Main Container -->
    <div class="container">
      <h3><fmt:message key="header.register"/></h3>
      <div class="row">
        <div class="col-md-5 col-md-push-3 col-sm-6 col-xs-12">
          <!--  Form -->
          <div class="form-grid">
           <form id="login-form" class="form" action="final-project-epam" method="post">
              <div class="form-group">
                <label><fmt:message key="registration_page.login"/></label>
                <input placeholder="Your Login" class="form-control" required  autofocus value="" name="login" type="text">
              </div>

              <div class="form-group">
                  <label>Email</label>
                <input placeholder="Your Email" class="form-control" required  value="" name="email" type="email">
              </div>
              <div class="form-group">
                 <label><fmt:message key="registration_page.phone"/></label>
                 <input placeholder="Your Phone" class="form-control" required name="phone" id="phone" value="" type="number">
              </div>
              <div class="form-group">
                <label>First Name</label>
                <input placeholder="Your First Name" class="form-control" required  value="" name="firstName" type="text">
              </div>

               <div class="form-group">
                <label>Last Name</label>
                <input placeholder="Your Last Name" class="form-control" required  value="" name="secondName" type="text">
              </div>
              <div class="form-group">
                <label><fmt:message key="registration_page.subscription"/></label>
                <input placeholder="<fmt:message key="registration_page.subscription"/>" class="form-control" required name="subscriptionId" value="" type="number">
              </div>

              <div class="form-group">
                <label><fmt:message key="registration_page.password"/></label>
                <input placeholder="Password" class="form-control" required name="password" value="${password}" type="password">
              </div>
              <div class="form-group">
                <label><fmt:message key="registration_page.password_confirmation"/></label>
                <input placeholder="Confirm Password" class="form-control" required name="confirmPassword" value="${confirmPassword}" type="password">
              </div>
              <div>${message}</div>
              <div style="color: red;"><p>

        <c:choose>
            <c:when test="${not empty requestScope.error}">
                <div class = "col-6">
                    <div class="alert alert-danger alert-dismissible">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                        ${error}
                    </div>
                </div>
            </c:when>
        </c:choose>
              </p></div>
                <input type="hidden" name="command" value="REGISTRATION"/>
                                                  <input type="submit" class="btn btn-theme btn-lg btn-block" value="<fmt:message key="registration_page.registration"/>"/>
            </form>
          </div>
          <!-- Form -->
        </div>

      </div>
      <!-- Row End -->
    </div>
    <!-- Main Container End -->
  </section>
  <!-- =-=-=-=-=-=-= Ads Archives End =-=-=-=-=-=-= -->
  <c:import url="parts/footer.jsp"/>
</div>
</body>
</html>