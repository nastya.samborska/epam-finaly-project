<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
    <title><fmt:message key="user_orders.title"/></title>
    <c:import url="parts/header.jsp" />
  </head>
  <%@ include file="parts/navigation.jsp" %> <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding gray">
      <!-- Main Container -->
      <div class="container">
        <h3>
           <fmt:message key="user_orders.title"/>
        </h3>
        <!-- Row -->
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- Row -->
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">

                           <table class="table table-striped">

                                                                        <c:choose>
                                                                            <c:when test="${empty userOrders && userOrders.size() == 0}">
                                                                                <tr>
                                                                                    <td><fmt:message key="catalog.not_found_message"/></td>
                                                                                </tr>
                                                                            </c:when>
                                                                            <c:when test="${not empty userOrders}">
                                                                                <tr>
                                                                                    <th>№</th>
                                                                                    <th><fmt:message key="user_orders.name_of_book"/></th>
                                                                                    <th><fmt:message key="user_orders.author"/></th>
                                                                                    <th><fmt:message key="user_orders.order_date"/></th>
                                                                                    <th><fmt:message key="user_orders.return_date"/></th>
                                                                                    <th><fmt:message key="user_orders.subscription"/></th>
                                                                                    <th><fmt:message key="user_orders.status"/></th>
                                                                                    <th></th>
                                                                                </tr>
                                                                                <c:forEach items="${sessionScope.userOrders}" var="order">
                                                                                    <tr>
                                                                                        <td>${order.id}</td>
                                                                                        <td>${order.book.name}</td>
                                                                                        <td>${order.book.author}</td>
                                                                                        <td>${order.dateOfIssue}</td>
                                                                                        <td>${order.returnDate}</td>
                                                                                        <c:if test="${order.subscription == true}">
                                                                                            <td>${order.account.subscriptionId}</td>
                                                                                        </c:if>
                                                                                        <c:if test="${order.subscription == false}">
                                                                                            <td>-</td>
                                                                                        </c:if>
                                                                                        <td>${order.orderStatus}</td>
                                                                                        <td class="rooms-table__action">
                                                                                            <c:if test="${order.orderStatus eq 'WAITING'}">
                                                                                                <a href="/final-project-epam/?command=CANCEL_ORDER&orderId=${order.id}"
                                                                                                   class="rooms-table__button">
                                                                                                    <fmt:message key="user_orders.cancel_button"/>
                                                                                                </a>
                                                                                            </c:if>
                                                                                        </td>
                                                                                    </tr>
                                                                                </c:forEach>
                                                                            </c:when>
                                                                        </c:choose>
                                                                    </table>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>