<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
   <title><fmt:message key="users.title"/></title>
    <c:import url="parts/header.jsp" />
  </head>
  <%@ include file="parts/navigation.jsp" %> <div class="small-breadcrumb">
    <div class="container">
      <div class=" breadcrumb-link">
        <ul>
          <li>&nbsp;</li>
        </ul>
      </div>
    </div>
  </div>
  <div class="main-content-area clearfix">
    <section class="section-padding gray">
      <!-- Main Container -->
      <div class="container">
        <h3>
           <fmt:message key="users.title"/>
        </h3>
        <!-- Row -->
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <!-- Row -->
            <div class="row">
              <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12 heading">
                         <table class="table table-striped">
                             <c:choose>
                                 <c:when test="${empty allUsers && allUsers.size() == 0}">
                                     <tr>
                                         <td><fmt:message key="catalog.not_found_message"/></td>
                                     </tr>
                                 </c:when>
                                 <c:when test="${not empty allUsers}">
                                     <tr>
                                         <th><fmt:message key="users.login"/></th>
                                         <th><fmt:message key="users.first_name"/></th>
                                         <th><fmt:message key="users.second_name"/></th>
                                         <th><fmt:message key="users.phone"/></th>
                                         <th><fmt:message key="users.subscription"/></th>
                                     </tr>
                                     <c:forEach items="${sessionScope.allUsers}" var="user">
                                         <tr>
                                             <td>${user.user.login}</td>
                                             <td>${user.firstName}</td>
                                             <td>${user.secondName}</td>
                                             <td>${user.phone}</td>
                                             <c:if test="${user.subscriptionId == null}">
                                                 <td>-</td>
                                             </c:if>
                                             <c:if test="${user.subscriptionId != null}">
                                                 <td>${user.subscriptionId}</td>
                                             </c:if>

                                         </tr>
                                     </c:forEach>
                                 </c:when>
                             </c:choose>
                         </table>
                  <c:if test="${empty allUsers && allUsers.size() == 0}"> No data </c:if>
                         <c:choose>
                              <c:when test="${not empty sessionScope.error}">
                                  <div class = "container p-3">
                                      <div class="alert alert-danger alert-dismissible" style="width: 500px; margin:auto">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          ${error}
                                      </div>
                                      <%session.setAttribute("error", "");%>
                                  </div>
                              </c:when>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.error}">
                                  <div class = "container p-3">
                                      <div class="alert alert-danger alert-dismissible" style="width: 500px; margin:auto">
                                          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                          ${error}
                                      </div>
                                  </div>
                              </c:when>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.successMakeOrder}">
                                  <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                              </c:when>
                              <c:otherwise>
                                  <c:if test="${sessionScope.successMakeOrder == true}">
                                      <div class = "container p-3">
                                          <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                              <fmt:message key="alert.success_make_order"/> "${sessionScope.orderNameBook}"!
                                          </div>
                                          <%session.setAttribute("successMakeOrder", false);%>
                                      </div>
                                  </c:if>
                              </c:otherwise>
                          </c:choose>
                          <c:choose>
                              <c:when test="${not empty requestScope.successEditBook}">
                                  <a href="${pageContext.request.contextPath}/controller?command=SHOW_ALL_BOOKS">Try again</a>
                              </c:when>
                              <c:otherwise>
                                  <c:if test="${sessionScope.successEditBook == true}">
                                      <div class = "container p-3">
                                          <div class="alert alert-success alert-dismissible" style="width: 500px; margin:auto">
                                              <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                              <fmt:message key="alert.success_edit_book"/>
                                          </div>
                                          <%session.setAttribute("successEditBook", false);%>
                                      </div>
                                  </c:if>
                              </c:otherwise>
                          </c:choose>
                </div>
              </div>
              <div class="clearfix"></div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <c:import url="parts/footer.jsp" />
  </div>
  </body>
</html>