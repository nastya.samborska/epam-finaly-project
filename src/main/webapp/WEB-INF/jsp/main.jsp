<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
    <title>
      <fmt:message key="main_page.title" />
    </title>
   <c:import url="parts/header.jsp"/>
  </head>
  <body>
     <%@ include file="parts/navigation.jsp" %>
    <div class="main-content-area clearfix">
      <section class="custom-padding gray">
        <p><img src="books.png" alt="" align="right"></p>
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
              <div class="about-us-content">
                <div class="heading-panel">
                  <h3 class="main-title text-left"> About us </h3>
                </div>
                <p>A library is a collection of materials, books or media that are accessible for use and not just for display purposes.</p>
                <p>A library provides physical (hard copies) or digital access (soft copies) materials, and may be a physical location or a virtual space, or both.</p>
                <p>A library's collection can include printed materials and other physical resources in many formats such as DVD, CD and cassette as well as access to information, music or other content held on bibliographic databases.</p>
                <p>A library, which may vary widely in size, may be organized for use and maintained by a public body such as a government; an institution such as a school or museum; a corporation; or a private individual. In addition to providing materials, libraries also provide the services of librarians who are trained and experts at finding, selecting, circulating and organizing information and at interpreting information needs, navigating and analyzing very large amounts of information with a variety of resources.</p>
              </div>
            </div>
          </div>
        </div>
      </section>
      <c:import url="parts/footer.jsp"/>
    </div>
  </body>
</html>