<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
  <c:choose>
    <c:when test="${not empty language}">
      <fmt:setLocale value="${sessionScope.language}" />
    </c:when>
    <c:when test="${empty language}">
      <fmt:setLocale value="en" />
    </c:when>
  </c:choose>
  <fmt:setBundle basename="language" />
  <head>
  <title><fmt:message key="error404.title"/></title>
   <c:import url="parts/header.jsp"/>
  </head>
  <body>
     <%@ include file="parts/navigation.jsp" %>
    <div class="main-content-area clearfix">
      <section class="custom-padding gray">
        <div class="container">
          <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-7 col-xs-12">
              <div class="about-us-content">
                <div class="heading-panel">
                </div><h1 style="text-align: center; width: 100%; min-height: 300px;">404 Not found</h1></div>
            </div>
          </div>
        </div>
      </section>
      <c:import url="parts/footer.jsp"/>
    </div>
  </body>
</html>