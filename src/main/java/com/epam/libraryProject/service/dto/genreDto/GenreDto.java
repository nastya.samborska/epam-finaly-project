package com.epam.libraryProject.service.dto.genreDto;

import com.epam.libraryProject.repository.entity.book.Genre;
import com.epam.libraryProject.service.dto.EntityDto;
import com.epam.libraryProject.service.dto.bookdto.BookDto;

import java.util.Objects;

public class GenreDto extends EntityDto<Integer> {
    private String name;

    public GenreDto(Integer id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GenreDto genreDto = (GenreDto) o;
        return Objects.equals(name, genreDto.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "GenreDto{" +
                "name='" + name + '\'' +
                '}';
    }


    public static class GenreDtoBuilder {

        private Integer id;
        private String name;

        public GenreDtoBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public GenreDtoBuilder withName(String name) {
            this.name = name;
            return this;
        }


        public GenreDto build() {
            GenreDto genreDto = new GenreDto(this.id);
            genreDto.setName(this.name);
            return genreDto;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GenreDtoBuilder that = (GenreDtoBuilder) o;
            return Objects.equals(id, that.id) &&
                    Objects.equals(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name);
        }

        @Override
        public String toString() {
            return "GenreDtoBuilder{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

}