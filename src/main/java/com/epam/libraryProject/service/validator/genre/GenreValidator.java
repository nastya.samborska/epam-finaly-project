package com.epam.libraryProject.service.validator.genre;

import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;
import com.epam.libraryProject.service.validator.Validator;

import static com.epam.libraryProject.service.exception.ExceptionMessage.INVALID_AUTHOR_EXCEPTION;
import static com.epam.libraryProject.service.exception.ExceptionMessage.STR_EMPTY_OR_NULL_EXCEPTION;
import static com.epam.libraryProject.service.exception.ExceptionMessage.INVALID_GENRE_NAME_EXCEPTION;

public class GenreValidator implements Validator<GenreDto, Integer> {

    private static final String NAME_REGEX = "^[a-zA-Z\\s]{3,40}$";

    @Override
    public void validate(GenreDto genreDto) throws ServiceException {
        isValidateName(genreDto.getName());
    }

    private void isValidateName(String name) throws ServiceException {
        isEmptyOrNull(name);
        if (!name.matches(NAME_REGEX)) {
            throw new ServiceException(INVALID_GENRE_NAME_EXCEPTION);
        }
    }

    private void isEmptyOrNull(String str) throws ServiceException {
        if (str == null || str.isEmpty()) {
            throw new ServiceException(STR_EMPTY_OR_NULL_EXCEPTION);
        }
    }
}
