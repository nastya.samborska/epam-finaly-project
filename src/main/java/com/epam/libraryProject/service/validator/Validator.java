package com.epam.libraryProject.service.validator;

import com.epam.libraryProject.service.dto.EntityDto;
import com.epam.libraryProject.service.exception.ServiceException;

public interface Validator<T extends EntityDto<K>, K> {
    void validate(T entity) throws ServiceException;
}
