package com.epam.libraryProject.service.exception;

public class IncorrectSignInParametersException extends Exception {
    public IncorrectSignInParametersException(String message) {
        super(message);
    }

}
