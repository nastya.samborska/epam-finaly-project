package com.epam.libraryProject.service.exception;

public class PasswordNotConfirmedException extends Exception {
    public PasswordNotConfirmedException(String message) {
        super(message);
    }
}
