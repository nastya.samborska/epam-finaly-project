package com.epam.libraryProject.service.exception;

public class LoginNotUniqueException extends Exception {
    public LoginNotUniqueException(String message) {
        super(message);
    }
}
