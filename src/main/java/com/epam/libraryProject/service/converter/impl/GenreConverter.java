package com.epam.libraryProject.service.converter.impl;

import com.epam.libraryProject.repository.entity.genre.Genre;
import com.epam.libraryProject.service.converter.Converter;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;

public class GenreConverter implements Converter<Genre, GenreDto, Integer> {
    @Override
    public Genre convert(GenreDto entity) {
        return new Genre.GenreBuilder()
                .withName(entity.getName())
                .build();
    }

    @Override
    public GenreDto convert(Genre entity) {
        return new GenreDto.GenreDtoBuilder()
                .withId(entity.getId())
                .withName(entity.getName())
                .build();
    }
}
