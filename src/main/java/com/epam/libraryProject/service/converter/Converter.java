package com.epam.libraryProject.service.converter;

import com.epam.libraryProject.repository.entity.Entity;
import com.epam.libraryProject.repository.exception.DaoException;
import com.epam.libraryProject.service.dto.EntityDto;

public interface Converter<T extends Entity<K>, V extends EntityDto<K>, K> {
    T convert(V entity);
    V convert(T entity) throws DaoException;
}
