package com.epam.libraryProject.service.converter.impl;

import com.epam.libraryProject.repository.api.AccountDao;
import com.epam.libraryProject.repository.api.BookDao;
import com.epam.libraryProject.repository.entity.book.Book;
import com.epam.libraryProject.repository.entity.order.Order;
import com.epam.libraryProject.repository.entity.user.Account;
import com.epam.libraryProject.repository.exception.DaoException;
import com.epam.libraryProject.repository.api.impl.AccountDaoImpl;
import com.epam.libraryProject.repository.api.impl.BookDaoImpl;
import com.epam.libraryProject.service.converter.Converter;
import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.orderdto.OrderDto;
import com.epam.libraryProject.service.dto.userdto.AccountDto;


public class OrderConverter implements Converter<Order, OrderDto, Integer> {
    private final BookDao bookDao = BookDaoImpl.getInstance();
    private final Converter<Book, BookDto, Integer> bookConverter = new BookConverter();
    private final AccountDao accountDao = AccountDaoImpl.getInstance();
    private final Converter<Account, AccountDto, Integer> accountConverter = new AccountConverter();

    @Override
    public Order convert(OrderDto entity) {
        return new Order.OrderBuilder()
                .withId(entity.getId())
                .withOrderStatus(entity.getOrderStatus())
                .withAccountId(entity.getAccount().getId())
                .withBookId(entity.getBook().getId())
                .withDateOfIssue(entity.getDateOfIssue())
                .withReturnDate(entity.getReturnDate())
                .withSubscription(entity.isSubscription())
                .build();
    }

    @Override
    public OrderDto convert(Order entity) throws DaoException {
        Book book = null;
        Account account = null;
        if (bookDao.findById(entity.getBookId()).isPresent()
            && accountDao.findById(entity.getAccountId()).isPresent()) {
            book = bookDao.findById(entity.getBookId()).get();
            account = accountDao.findById(entity.getAccountId()).get();
        }
        return new OrderDto.OrderDtoBuilder()
                .withId(entity.getId())
                .withOrderStatus(entity.getOrderStatus())
                .withAccount(accountConverter.convert(account))
                .withBook(bookConverter.convert(book))
                .withDateOfIssue(entity.getDateOfIssue())
                .withReturnDate(entity.getReturnDate())
                .withSubscription(entity.isSubscription())
                .build();
    }
}
