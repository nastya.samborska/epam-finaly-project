package com.epam.libraryProject.service.api.impl;

import com.epam.libraryProject.repository.api.BookDao;
import com.epam.libraryProject.repository.api.GenreDao;
import com.epam.libraryProject.repository.api.impl.BookDaoImpl;
import com.epam.libraryProject.repository.api.impl.GenreDaoImpl;
import com.epam.libraryProject.repository.entity.book.Book;
import com.epam.libraryProject.repository.entity.genre.Genre;
import com.epam.libraryProject.repository.exception.DaoException;
import com.epam.libraryProject.service.api.GenreService;
import com.epam.libraryProject.service.converter.Converter;
import com.epam.libraryProject.service.converter.impl.BookConverter;
import com.epam.libraryProject.service.converter.impl.GenreConverter;
import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;
import com.epam.libraryProject.service.validator.book.BookValidator;
import com.epam.libraryProject.service.validator.genre.GenreValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.libraryProject.service.exception.ExceptionMessage.*;
import static com.epam.libraryProject.service.exception.ExceptionMessage.SERVICE_FIND_BY_ID_METHOD_EXCEPTION;

public class GenreServiceImpl implements GenreService {
    private final GenreDao genreDao;
    private final Converter<Genre, GenreDto, Integer> converter;
    private final GenreValidator validator;
    private static GenreServiceImpl instance = new GenreServiceImpl();
    private static final Logger log = LogManager.getLogger(GenreServiceImpl.class);
    private static final String NAME_SORT_PARAM = "name";
    private static final String QUANTITY_SORT_PARAM = "quantity";

    private GenreServiceImpl() {
        this.genreDao = GenreDaoImpl.getInstance();
        this.converter = new GenreConverter();
        this.validator = new GenreValidator();
    }

    public static GenreServiceImpl getInstance() {
        return instance;
    }

    @Override
    public GenreDto create(GenreDto genreDto) throws ServiceException {
        validator.validate(genreDto);
        Genre createdGenre = converter.convert(genreDto);
        try {
            genreDto = converter.convert(genreDao.add(createdGenre));
        } catch (DaoException e) {
            log.error(SERVICE_CREATE_METHOD_EXCEPTION, e);
            throw new ServiceException(SERVICE_CREATE_METHOD_EXCEPTION, e);
        }
        return genreDto;
    }

    @Override
    public List<GenreDto> findAll() throws ServiceException {
        List<GenreDto> genreDto = new ArrayList<>();
        try {
            for (Genre genre : genreDao.findAll()) {
                genreDto.add(converter.convert(genre));
            }
        } catch (DaoException e) {
            log.error(SERVICE_FIND_ALL_METHOD_EXCEPTION, e);
            throw new ServiceException(SERVICE_FIND_ALL_METHOD_EXCEPTION, e);
        }
        return genreDto;
    }
    @Override
    public Optional<GenreDto> findById(Integer id) throws ServiceException {
        Optional<GenreDto> genreDtoOptional = Optional.empty();
        try {
            Optional<Genre> genre = genreDao.findById(id);
            if (genre.isPresent()) {
                GenreDto genreDto = converter.convert(genre.get());
                genreDtoOptional = Optional.of(genreDto);
            }
        } catch (DaoException e) {
            log.error(SERVICE_FIND_BY_ID_METHOD_EXCEPTION, e);
            throw new ServiceException(SERVICE_FIND_BY_ID_METHOD_EXCEPTION, e);
        }
        return genreDtoOptional;
    }


    @Override
    public void editGenre(GenreDto genre, Integer bookId) throws ServiceException {
        validator.validate(genre);
        try {
            genreDao.updateGenreById(converter.convert(genre), bookId);
        } catch (DaoException e) {
            log.error(SERVICE_UPDATE_METHOD_EXCEPTION, e);
            throw new ServiceException(SERVICE_UPDATE_METHOD_EXCEPTION, e);
        }
    }

}
