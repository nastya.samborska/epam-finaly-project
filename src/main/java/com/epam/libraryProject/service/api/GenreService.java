package com.epam.libraryProject.service.api;

import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;

import java.util.List;

public interface GenreService extends Service<GenreDto, Integer>  {
    GenreDto create(GenreDto genreDto) throws ServiceException;
    void editGenre(GenreDto genre, Integer genreId) throws ServiceException;

}
