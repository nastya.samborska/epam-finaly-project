package com.epam.libraryProject.service.api;

import com.epam.libraryProject.service.dto.EntityDto;
import com.epam.libraryProject.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public interface Service<T extends EntityDto<K>, K> {
    List<T> findAll() throws ServiceException;
    Optional<T> findById(K id) throws ServiceException;
}
