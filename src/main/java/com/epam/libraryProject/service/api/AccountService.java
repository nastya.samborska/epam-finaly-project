package com.epam.libraryProject.service.api;

import com.epam.libraryProject.service.dto.userdto.AccountDto;
import com.epam.libraryProject.service.exception.ServiceException;

import java.util.List;
import java.util.Optional;

public interface AccountService extends Service<AccountDto, Integer> {
    AccountDto create(AccountDto accountDto) throws ServiceException;
    Optional<AccountDto> findByUserId(Integer userId) throws ServiceException;
    List<AccountDto> findUsersOnPage(int page, int totalUsersOnPage) throws ServiceException;
    void editAccount(String firstName, String secondName, String phone,
                     String subscriptionId, Integer accountId) throws ServiceException;
}
