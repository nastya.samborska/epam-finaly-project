package com.epam.libraryProject.controller.command.api;

public interface CommandResponse {
    String getPath();
    boolean isRedirect();
}
