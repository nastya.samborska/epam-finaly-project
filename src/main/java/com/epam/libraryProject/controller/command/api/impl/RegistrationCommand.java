package com.epam.libraryProject.controller.command.api.impl;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.repository.entity.user.UserRole;
import com.epam.libraryProject.service.api.AccountService;
import com.epam.libraryProject.service.dto.userdto.AccountDto;
import com.epam.libraryProject.service.dto.userdto.UserDto;
import com.epam.libraryProject.service.exception.LoginNotUniqueException;
import com.epam.libraryProject.service.exception.PasswordNotConfirmedException;
import com.epam.libraryProject.service.exception.ServiceException;
import com.epam.libraryProject.service.api.impl.AccountServiceImpl;
import com.epam.libraryProject.service.api.impl.UserServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;

public class RegistrationCommand implements Command {
    private static final Logger log = LogManager.getLogger(RegistrationCommand.class);
    private final UserServiceImpl userService = UserServiceImpl.getInstance();
    private final AccountService accountService = AccountServiceImpl.getInstance();
    private static final Command instance = new RegistrationCommand();
    private static final String LOGIN_PAGE = "/final-project-epam/?command=SHOW_LOGIN_PAGE";
    private static final String ERROR_MESSAGE = "Registration failed";
    private static final String ERROR_ATTRIBUTE = "error";

    private RegistrationCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse REGISTRATION_SUCCESS_RESPONSE = new CommandResponse() {
        @Override
        public String getPath() {
            return LOGIN_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse REGISTRATION_ERROR_RESPONSE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.REGISTRATION_PAGE_PATH;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        String login = request.getParameter(LOGIN);
        String password = request.getParameter(PASSWORD);
        String email = request.getParameter(EMAIL);
        String confirmPassword = request.getParameter(CONFIRM_PASSWORD);
        String firstName = request.getParameter(ACCOUNT_NAME);
        String secondName = request.getParameter(ACCOUNT_SECOND_NAME);
        String phone = request.getParameter(ACCOUNT_PHONE);
        String subscriptionId = request.getParameter(ACCOUNT_SUBSCRIPTION);

        try {
            UserDto registerUser = new UserDto(login, password);
            registerUser.setRole(UserRole.USER);
            UserDto user = userService.create(registerUser, confirmPassword);
            AccountDto registerAccount = creteAccount(user, firstName, secondName, phone, subscriptionId);
            accountService.create(registerAccount);

        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE, e);
            request.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
            return REGISTRATION_ERROR_RESPONSE;
        } catch (LoginNotUniqueException | PasswordNotConfirmedException e) {
            request.setAttribute(ERROR_ATTRIBUTE, e.getMessage());
            return REGISTRATION_ERROR_RESPONSE;
        }
        return successRegistration(request, true);
    }

    private CommandResponse successRegistration(CommandRequest request, boolean isSuccessRegister) {
        request.getCurrentSession().ifPresent(HttpSession::invalidate);
        final HttpSession session = request.createSession();
        session.setAttribute("successRegister", isSuccessRegister);
        return REGISTRATION_SUCCESS_RESPONSE;
    }

    private AccountDto creteAccount(UserDto user, String firstName, String secondName,
                                    String phone, String subscriptionId) {
        return new AccountDto.AccountDtoBuilder()
                        .withUser(user)
                        .withFirstName(firstName)
                        .withSecondName(secondName)
                        .withPhone(phone)
                        .withSubscriptionId(subscriptionId)
                        .build();

    }
}
