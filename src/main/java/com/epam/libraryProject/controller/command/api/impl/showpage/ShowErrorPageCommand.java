package com.epam.libraryProject.controller.command.api.impl.showpage;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;

public class ShowErrorPageCommand implements Command {
    private static final Command instance = new ShowErrorPageCommand();

    private ShowErrorPageCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse ERROR_RESPONSE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        return ERROR_RESPONSE;
    }
}
