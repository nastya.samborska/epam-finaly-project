package com.epam.libraryProject.controller.command.api.impl;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.service.dto.userdto.AccountDto;
import com.epam.libraryProject.service.exception.ServiceException;
import com.epam.libraryProject.service.api.impl.AccountServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;

import java.util.Optional;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;

public class EditAccountCommand implements Command {
    private static final Command instance = new EditAccountCommand();
    private static final Logger log = LogManager.getLogger(EditAccountCommand.class);
    private static final AccountServiceImpl accountService = AccountServiceImpl.getInstance();
    private static final String EDIT_ACCOUNT_SUCCESS = "successEditAccount";
    private static final String EDIT_PAGE = "/final-project-epam?command=SHOW_EDIT_ACCOUNT_PAGE";
    private static final String MAIN_PAGE = "/final-project-epam?command=SHOW_MAIN_PAGE";
    private static final String ERROR_MESSAGE = "Can't edit account";
    private static final String ERROR_ATTRIBUTE = "error";

    private EditAccountCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse SHOW_EDIT_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return EDIT_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse SHOW_MAIN_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return MAIN_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse ERROR_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        HttpSession session;
        if (request.getCurrentSession().isPresent()) {
            session = request.getCurrentSession().get();
        } else {
            return ERROR_PAGE;
        }
        String firstName = request.getParameter(ACCOUNT_NAME);
        String secondName = request.getParameter(ACCOUNT_SECOND_NAME);
        String phone = request.getParameter(ACCOUNT_PHONE);
        String subscriptionId = request.getParameter(ACCOUNT_SUBSCRIPTION);
        AccountDto currentAccount =
                (AccountDto) session.getAttribute(USER_ACCOUNT_SESSION_ATTRIB_NAME);

        try {
            accountService.editAccount(firstName, secondName, phone, subscriptionId, currentAccount.getId());
            session.setAttribute(EDIT_ACCOUNT_SUCCESS, true);
            Optional<AccountDto> foundAccount = accountService.findById(currentAccount.getId());
            if (foundAccount.isPresent()) {
                AccountDto updateAccount = foundAccount.get();
                session.setAttribute(USER_ACCOUNT_SESSION_ATTRIB_NAME, updateAccount);
            } else {
                log.error(ERROR_MESSAGE);
                session.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
                return SHOW_MAIN_PAGE;
            }
        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE);
            session.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
            return SHOW_MAIN_PAGE;
        }
        return SHOW_EDIT_PAGE;
    }
}
