package com.epam.libraryProject.controller.command.api.impl.showpage;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.service.api.impl.GenreServiceImpl;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;
import java.util.List;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;

public class ShowAllGenresCommand implements Command {
    private final GenreServiceImpl genreService = GenreServiceImpl.getInstance();
private static final Command instance = new ShowAllGenresCommand();
    private static final Logger log = LogManager.getLogger(ShowAllGenresCommand.class);
    private static final String PAGE_ATTRIBUTE = "page";
    private static final Integer TOTAL_ORDER_ON_PAGE = 7;
    private static final Integer START_PAGE = 1;
    private static final String ERROR_MESSAGE = "Can't find genres";
    private static final String ERROR_ATTRIBUTE = "error";

    private ShowAllGenresCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse SHOW_GENRES = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ALL_GENRES_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    private static final CommandResponse ERROR_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        HttpSession session;
        if (request.getCurrentSession().isPresent()) {
            session = request.getCurrentSession().get();
        } else {
            return ERROR_PAGE;
        }

       /* int page;
        if (request.getParameter(PAGE_ATTRIBUTE) == null
                || Integer.parseInt(request.getParameter(PAGE_ATTRIBUTE)) <= 1) {
            page = START_PAGE;
        } else {
            page = Integer.parseInt(request.getParameter(PAGE_ATTRIBUTE));
        }

        request.setAttribute(PAGE_ATTRIBUTE, page);

        if (page != 1) {
            page = page - 1;
            page = page * TOTAL_ORDER_ON_PAGE + 1;
        }*/

        try {
            List<GenreDto> allGenres = genreService.findAll();
            session.setAttribute(ALL_GENRES, allGenres);
        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE, e);
            request.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
        }
        return SHOW_GENRES;
    }
}
