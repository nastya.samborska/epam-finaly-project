package com.epam.libraryProject.controller.command.api;

import com.epam.libraryProject.controller.command.CommandsName;

public interface Command {
    CommandResponse execute(CommandRequest request);

    static Command withName(String name) {
        return CommandsName.of(name)
                .getCommand();
    }
}
