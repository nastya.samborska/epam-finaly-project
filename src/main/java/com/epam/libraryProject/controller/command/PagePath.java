package com.epam.libraryProject.controller.command;

public class PagePath {
    public static final String LOGIN_PAGE_PATH = "WEB-INF/jsp/signIn.jsp";
    public static final String ERROR_404 = "WEB-INF/jsp/error404.jsp";
    public static final String CATALOG_PAGE_PATH = "WEB-INF/jsp/catalog.jsp";
    public static final String REGISTRATION_PAGE_PATH = "WEB-INF/jsp/registration.jsp";
    public static final String USER_ORDERS_PAGE = "WEB-INF/jsp/userOrders.jsp";
    public static final String ALL_USERS_PAGE = "WEB-INF/jsp/allUsers.jsp";
    public static final String ADD_BOOK_PAGE = "WEB-INF/jsp/addBook.jsp";
    public static final String ALL_ORDERS_PAGE = "WEB-INF/jsp/allOrders.jsp";
    public static final String EDIT_BOOK_PAGE = "WEB-INF/jsp/editBook.jsp";
    public static final String EDIT_ACCOUNT_PAGE = "WEB-INF/jsp/editAccount.jsp";

    public static final String ADD_GENRE_PAGE = "WEB-INF/jsp/addGenre.jsp";
    public static final String EDIT_GENRE_PAGE = "WEB-INF/jsp/editGenre.jsp";
    public static final String ALL_GENRES_PAGE = "WEB-INF/jsp/allGenres.jsp";

    private PagePath() {
    }
}
