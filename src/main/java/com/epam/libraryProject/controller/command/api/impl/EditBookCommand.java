package com.epam.libraryProject.controller.command.api.impl;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.repository.entity.book.Genre;
import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.exception.ServiceException;
import com.epam.libraryProject.service.api.impl.BookServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;

public class EditBookCommand implements Command {
    private static final Command instance = new EditBookCommand();
    private static final Logger log = LogManager.getLogger(EditBookCommand.class);
    private static final BookServiceImpl bookService = BookServiceImpl.getInstance();
    private static final String EDIT_BOOK_SUCCESS = "successEditBook";
    private static final String CATALOG_PAGE = "/final-project-epam/?command=SHOW_ALL_BOOKS";
    private static final String ERROR_MESSAGE = "Can't edit book";
    private static final String ERROR_ATTRIBUTE = "error";

    private EditBookCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse SHOW_CATALOG_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return CATALOG_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse ERROR_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        HttpSession session;
        if (request.getCurrentSession().isPresent()) {
            session = request.getCurrentSession().get();
        } else {
            return ERROR_PAGE;
        }

        String author = request.getParameter(BOOK_AUTHOR);
        String name = request.getParameter(BOOK_NAME);
        String publisher = request.getParameter(BOOK_PUBLISHER);
        int genreId = Integer.parseInt(request.getParameter(BOOK_GENRE));
        Genre genre = Genre.getGenreById(genreId);
        int yearPublishing = Integer.parseInt(request.getParameter(BOOK_YEAR));
        int pages = Integer.parseInt(request.getParameter(BOOK_PAGES));
        int quantity = Integer.parseInt(request.getParameter(BOOK_QUANTITY));

        BookDto book = createBook(author, name, publisher, genre, yearPublishing, pages, quantity);
        try {
            String bookId = (String) session.getAttribute(BOOK_ID);
            bookService.editBook(book, Integer.parseInt(bookId));

            session.setAttribute(EDIT_BOOK_SUCCESS, true);
            request.setAttribute(BOOK_ID, bookId);

        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE, e);
            session.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
            return SHOW_CATALOG_PAGE;
        }
        return SHOW_CATALOG_PAGE;
    }

    private BookDto createBook(String author, String name, String publisher, Genre genre, int year,
                               int pages, int quantity) {
        return new BookDto.BookDtoBuilder()
                .withAuthor(author)
                .withName(name)
                .withPublishingHouse(publisher)
                .withGenre(genre)
                .withYearPublishing(year)
                .withNumberOfPage(pages)
                .withQuantity(quantity)
                .build();
    }
}
