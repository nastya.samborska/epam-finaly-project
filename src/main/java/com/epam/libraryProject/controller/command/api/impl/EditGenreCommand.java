package com.epam.libraryProject.controller.command.api.impl;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.repository.entity.book.Genre;
import com.epam.libraryProject.service.api.impl.BookServiceImpl;
import com.epam.libraryProject.service.api.impl.GenreServiceImpl;
import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;
import static com.epam.libraryProject.controller.command.RequestParameterName.BOOK_ID;

public class EditGenreCommand implements Command {
    private static final Command instance = new EditGenreCommand();
    private static final Logger log = LogManager.getLogger(EditGenreCommand.class);
    private static final GenreServiceImpl genreService = GenreServiceImpl.getInstance();
    private static final String EDIT_GENRE_SUCCESS = "successEditGenre";
    private static final String CATALOG_PAGE = "?command=SHOW_ALL_GENRES";
    private static final String ERROR_MESSAGE = "Can't edit genre";
    private static final String ERROR_ATTRIBUTE = "error";

    private EditGenreCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse SHOW_CATALOG_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return CATALOG_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse ERROR_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        HttpSession session;
        if (request.getCurrentSession().isPresent()) {
            session = request.getCurrentSession().get();
        } else {
            return ERROR_PAGE;
        }

        String name = request.getParameter(GENRE_NAME);

        GenreDto genre = createGenre(name);
        try {
            String genreId = (String) session.getAttribute(GENRE_ID);
            genreService.editGenre(genre, Integer.parseInt(genreId));

            session.setAttribute(EDIT_GENRE_SUCCESS, true);
            request.setAttribute(GENRE_ID, genreId);

        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE, e);
            session.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
            return SHOW_CATALOG_PAGE;
        }
        return SHOW_CATALOG_PAGE;
    }

    private GenreDto createGenre(String name) {
        return new GenreDto.GenreDtoBuilder()
                .withName(name)
                .build();
    }
}
