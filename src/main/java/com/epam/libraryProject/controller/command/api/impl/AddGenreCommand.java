package com.epam.libraryProject.controller.command.api.impl;

import com.epam.libraryProject.controller.command.PagePath;
import com.epam.libraryProject.controller.command.api.Command;
import com.epam.libraryProject.controller.command.api.CommandRequest;
import com.epam.libraryProject.controller.command.api.CommandResponse;
import com.epam.libraryProject.repository.entity.book.Genre;
import com.epam.libraryProject.service.api.impl.BookServiceImpl;
import com.epam.libraryProject.service.api.impl.GenreServiceImpl;
import com.epam.libraryProject.service.dto.bookdto.BookDto;
import com.epam.libraryProject.service.dto.genreDto.GenreDto;
import com.epam.libraryProject.service.exception.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpSession;

import static com.epam.libraryProject.controller.command.RequestParameterName.*;

public class AddGenreCommand implements Command {

    private static final Command instance = new AddGenreCommand();
    private static final Logger log = LogManager.getLogger(AddGenreCommand.class);
    private static final String ADD_GENRE_SUCCESS = "successAddGenre";
    private static final String ADD_GENRE_PAGE = "/final-project-epam/?command=SHOW_ADD_GENRE_PAGE";
    private final GenreServiceImpl genreService = GenreServiceImpl.getInstance();
    private static final String ERROR_MESSAGE = "Can't add genre";
    private static final String ERROR_ATTRIBUTE = "error";

    private AddGenreCommand() {
    }

    public static Command getInstance() {
        return instance;
    }

    private static final CommandResponse SHOW_ADD_GENRE_PAGE= new CommandResponse() {
        @Override
        public String getPath() {
            return ADD_GENRE_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return true;
        }
    };

    private static final CommandResponse FAIL_ADD_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ADD_GENRE_PAGE;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    private static final CommandResponse ERROR_PAGE = new CommandResponse() {
        @Override
        public String getPath() {
            return PagePath.ERROR_404;
        }

        @Override
        public boolean isRedirect() {
            return false;
        }
    };

    @Override
    public CommandResponse execute(CommandRequest request) {
        HttpSession session;

        if (request.getCurrentSession().isPresent()) {
            session = request.getCurrentSession().get();
        } else {
            return ERROR_PAGE;
        }

        String name = request.getParameter(GENRE_NAME);

        GenreDto genre = createGenre(name);

        try {
            genreService.create(genre);
            session.setAttribute(ADD_GENRE_SUCCESS, true);
        } catch (ServiceException e) {
            log.error(ERROR_MESSAGE, e);
            request.setAttribute(ERROR_ATTRIBUTE, ERROR_MESSAGE);
            return FAIL_ADD_PAGE;
        }
    return SHOW_ADD_GENRE_PAGE;
    }

    private GenreDto createGenre(String name) {
        return new GenreDto.GenreDtoBuilder()
                .withName(name)
                .build();
    }
}
