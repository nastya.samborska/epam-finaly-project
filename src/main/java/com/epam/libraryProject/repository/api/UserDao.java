package com.epam.libraryProject.repository.api;

import com.epam.libraryProject.repository.entity.user.User;
import com.epam.libraryProject.repository.exception.DaoException;

import java.util.Optional;

public interface UserDao extends BaseDao<User, Integer> {
    String findPasswordByLogin(String login) throws DaoException;
    void updatePasswordByLogin(String password, String login) throws DaoException;
    Optional<User> findByLogin(String login) throws DaoException;
}
