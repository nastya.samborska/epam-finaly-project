package com.epam.libraryProject.repository.api;

import com.epam.libraryProject.repository.entity.book.Book;
import com.epam.libraryProject.repository.entity.genre.Genre;
import com.epam.libraryProject.repository.exception.DaoException;

public interface GenreDao extends BaseDao<Genre, Integer>  {
    void updateGenreById(Genre genre, Integer genreId) throws DaoException;
}
