package com.epam.libraryProject.repository.api.impl;

import com.epam.libraryProject.repository.api.GenreDao;
import com.epam.libraryProject.repository.connection.api.ConnectionPool;
import com.epam.libraryProject.repository.connection.api.impl.ConnectionPoolImpl;
import com.epam.libraryProject.repository.entity.book.Book;
import com.epam.libraryProject.repository.entity.genre.Genre;
import com.epam.libraryProject.repository.exception.DaoException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static com.epam.libraryProject.repository.exception.ExceptionMessage.*;
import static com.epam.libraryProject.repository.exception.ExceptionMessage.FIND_BY_ID_EXCEPTION;

public class GenreDaoImpl implements GenreDao {
    private static GenreDaoImpl instance = new GenreDaoImpl();
    private ConnectionPool pool;
    private static final Logger log = LogManager.getLogger(GenreDaoImpl.class);

    private GenreDaoImpl() {
        this.pool = ConnectionPoolImpl.getInstance();
    }


    public static GenreDaoImpl getInstance() {
        return instance;
    }

    @Override
    public Genre add(Genre genre) throws DaoException {
        try (Connection connection = pool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(SqlQuery.ADD_GENRE,
                     Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, genre.getName());
            statement.executeUpdate();
            ResultSet resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                genre.setId(resultSet.getInt(1));
            }
        } catch (SQLException e) {
            log.error(CREATE_EXCEPTION, e);
            throw new DaoException(CREATE_EXCEPTION, e);
        }
        return genre;
    }

    @Override
    public boolean delete(Genre genre) throws DaoException {
        try (Connection connection = pool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(SqlQuery.DELETE_GENRE_BY_ID)) {
            statement.setInt(1, genre.getId());
            return statement.executeUpdate() > 0;
        } catch (SQLException e) {
            log.error(DELETE_EXCEPTION, e);
            throw new DaoException(DELETE_EXCEPTION, e);
        }
    }

    @Override
    public List<Genre> findAll() throws DaoException {
        List<Genre> allGenres;
        try (Connection connection = pool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(SqlQuery.FIND_ALL_GENRES)) {
             allGenres= findGenres(statement);
        } catch (SQLException e) {
            log.error(FIND_ALL_EXCEPTION, e);
            throw new DaoException(FIND_ALL_EXCEPTION, e);
        }
        return allGenres;
    }

    @Override
    public Optional<Genre> findById(Integer id) throws DaoException {
        Optional<Genre> genre = Optional.empty();
        try (Connection connection = pool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(SqlQuery.FIND_GENRE_BY_ID)) {
            statement.setInt(1, id);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                genre = Optional.of(createGenre(resultSet));
            }
        } catch (SQLException e) {
            log.error(FIND_BY_ID_EXCEPTION, e);
            throw new DaoException(FIND_BY_ID_EXCEPTION, e);
        }
        return genre;
    }

    @Override
    public void updateGenreById(Genre genre, Integer genreId) throws DaoException {
        try (Connection connection = pool.takeConnection();
             PreparedStatement statement = connection.prepareStatement(SqlQuery.UPDATE_GENRE_NAME_BY_ID)) {
            statement.setString(1, genre.getName());
            statement.setInt(2, genre.getId());
            statement.executeUpdate();
        } catch (SQLException e) {
            log.error(UPDATE_EXCEPTION, e);
            throw new DaoException(UPDATE_EXCEPTION, e);
        }
    }
    private List<Genre> findGenres(PreparedStatement statement) throws SQLException {
        List<Genre> allGenres = new ArrayList<>();
        try (statement; ResultSet resultSet = statement.executeQuery()) {
            while (resultSet.next()) {
                allGenres.add(createGenre(resultSet));
            }
        }
        return allGenres;
    }
    private Genre createGenre(ResultSet resultSet) throws SQLException{
        return new Genre.GenreBuilder()
                .withId(resultSet.getInt(1))
                .withName(resultSet.getString(2))
                .build();
    }

}
