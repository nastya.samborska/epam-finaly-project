package com.epam.libraryProject.repository.entity.genre;

import com.epam.libraryProject.repository.entity.Entity;

import java.util.Objects;

public class Genre extends Entity<Integer> {
    private String name;

    public Genre() {
    }

    public Genre(Integer id) {
        super(id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Genre genre = (Genre) o;
        return Objects.equals(name, genre.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return "Genre{" +
                "name='" + name + '\'' +
                '}';
    }

    public static class GenreBuilder {

        private Integer id;
        private String name;

        public GenreBuilder withId(Integer id) {
            this.id = id;
            return this;
        }

        public GenreBuilder withName(String name) {
            this.name = name;
            return this;
        }


        public Genre build() {
            Genre genre = new Genre(this.id);
            genre.setName(this.name);
            return genre;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GenreBuilder that = (GenreBuilder) o;
            return Objects.equals(id, that.id) &&
                    Objects.equals(name, that.name);
        }

        @Override
        public int hashCode() {
            return Objects.hash(id, name);
        }

        @Override
        public String toString() {
            return "GenreBuilder{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    '}';
        }
    }
}