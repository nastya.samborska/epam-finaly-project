package com.epam.libraryProject.repository.connection.api;

import com.epam.libraryProject.repository.exception.DaoException;

import java.sql.Connection;

public interface ConnectionPool {
    void init() throws DaoException;
    void destroy() throws DaoException;
    Connection takeConnection();
    void returnConnection(Connection connection);
}
